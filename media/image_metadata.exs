Mix.install(
  [
    {:ex_aws, "~> 2.2"},
    {:ex_aws_s3, "~> 2.3"},
    {:exexif, "~> 0.0.5"},
    {:hackney, "~> 1.18"},
    {:jason, "~> 1.3"},
    {:ecto_sqlite3, "~> 0.12"},
    {:sweet_xml, "~> 0.6"},
    {:image, "~> 0.38.3"}
  ],
  config: [
    ex_aws: [
      hackney_opts: [
        recv_timeout: 240_000
      ]
    ]
  ]
)

Application.put_env(:images, Repo, database: "./database.db")

defmodule Repo do
  use Ecto.Repo, otp_app: :images, adapter: Ecto.Adapters.SQLite3
end

defmodule Media.Image do
  use Ecto.Schema
  import Ecto.Changeset

  schema "images" do
    field(:datetime, :utc_datetime)
    field(:flash, :string)
    field(:contrast, :string)
    field(:focal_length, :float)
    field(:white_balance, :string)
    field(:exposure_time, :float)
    field(:width, :integer)
    field(:height, :integer)
    field(:iso, :integer)
    field(:aperture, :float)
    field(:orientation, :string)
    field(:hash, :string)
    field(:original_path, :string)
    field(:path, :string)
    field(:tags, {:array, :string})
    field(:uploaded, :boolean, default: false)
  end

  def changeset(image, attrs) do
    image
    |> cast(
      attrs,
      ~w[datetime flash contrast focal_length white_balance exposure_time width height iso aperture orientation hash original_path path tags uploaded]a
    )
  end

  def secs(0.0), do: "&infin;"
  def secs(time) when time >= 1.0, do: "#{time}\""

  def secs(time) do
    den = floor(1 / time)
    "<sup>1</sup>&frasl;<sub>#{den}</sub>\""
  end

  def thumb_path(%{path: path}) do
    [s, ext] = String.split(path, ".")
    "#{s}_thumb.#{ext}"
  end
end

defmodule Video do
  use Ecto.Schema
  import Ecto.Changeset

  schema "videos" do
    field(:datetime, :utc_datetime)
    field(:name, :string)
    field(:hash, :string)
    field(:original_path, :string)
    field(:path, :string)
    field(:uploaded, :boolean, default: false)
  end

  def changeset(image, attrs) do
    image
    |> cast(
      attrs,
      ~w[datetime name hash original_path path uploaded]a
    )
  end
end

defmodule Migration do
  use Ecto.Migration

  def change do
    create table("images") do
      add(:datetime, :utc_datetime)
      add(:flash, :string)
      add(:contrast, :string)
      add(:focal_length, :float)
      add(:white_balance, :string)
      add(:exposure_time, :float)
      add(:width, :integer)
      add(:height, :integer)
      add(:iso, :integer)
      add(:aperture, :float)
      add(:orientation, :string)
      add(:hash, :string)
      add(:original_path, :string)
      add(:path, :string)
      add(:tags, {:array, :string})
      add(:uploaded, :boolean, default: false)
    end
  end
end

defmodule Migration0 do
  use Ecto.Migration

  def change do
    create table("videos") do
      add(:datetime, :utc_datetime)
      add(:name, :string)
      add(:hash, :string)
      add(:original_path, :string)
      add(:path, :string)
      add(:uploaded, :boolean, default: false)
    end
  end
end

defmodule Videos do
  import Ecto.Query, warn: false

  def parse_root(root_path) do
    files =
      root_path
      |> File.ls!()

    Enum.flat_map(files, &parse_dir(&1, Path.join(root_path, &1)))
  end

  def parse_dir(dir, path) do
    project = Path.join(path, "project.mp4")
    thumb = Path.join(path, "thumbnail.jpg")

    if(File.dir?(path) and File.exists?(project) and File.exists?(thumb)) do
      stat = File.stat!(project)
      data = File.read!(project)
      hash = :crypto.hash(:md5, data)
      encoded = Base.encode16(hash, case: :lower)
      file = encoded <> ".mp4"

      v =
        case Repo.one(from(v in Video, where: v.original_path == ^project)) do
          nil -> %Video{}
          vi -> vi
        end

      cs =
        Video.changeset(v, %{
          name: dir,
          datetime: NaiveDateTime.from_erl!(stat.mtime),
          hash: encoded,
          original_path: project,
          path: "videos/#{file}"
        })

      cs =
        if Ecto.Changeset.changed?(cs, :datetime) do
          Ecto.Changeset.put_change(cs, :uploaded, false)
        else
          cs
        end

      [cs]
    else
      []
    end
  end

  def create_videos(videos, media_server) do
    f =
      EEx.eval_file("templates/videos.html.eex",
        videos: videos,
        media_server: media_server,
        current_tag: "videos"
      )

    File.write!("../www/videos.html", f)

    Enum.each(videos, fn v ->
      f =
        EEx.eval_file("templates/video.html.eex",
          video: v,
          media_server: media_server,
          current_tag: v.name
        )

      File.write!("../www/video/#{v.hash}.html", f)
    end)
  end

  def rss(videos, media_server) do
    items =
      Enum.map(videos, fn v ->
        %{
          title: v.name,
          description: description(v, media_server),
          link: link(v),
          hash: v.hash,
          datetime: v.datetime
        }
      end)

    f =
      EEx.eval_file("templates/rss.xml.eex",
        title: "Entropealabs Videos",
        description: "Videos produced by Entropealabs, LTD",
        link: "https://media.entropealabs.com/videos",
        items: items
      )

    File.write!("../www/videos.xml", f)
  end

  defp description(%{hash: hash, name: name}, media_server) do
    """
      <p>#{name}</p>
      <img src="#{media_server}/videos/#{hash}.jpg" />
    """
  end

  defp link(%{hash: hash}) do
    "https://media.entropealabs.com/video/#{hash}"
  end

  def upload_new(videos, bucket, region) do
    new = Enum.reject(videos, & &1.uploaded)

    Enum.each(new, fn %{original_path: src_path, path: dest_path, hash: hash} = record ->
      [_ | path] =
        src_path
        |> String.split("/")
        |> Enum.reverse()

      thumbnail = ["thumbnail.jpg" | path] |> Enum.reverse() |> Enum.join("/")
      thumbnail_data = File.read!(thumbnail)

      bucket
      |> ExAws.S3.put_object("videos/#{hash}.jpg", thumbnail_data,
        content_type: "image/jpeg",
        acl: :public_read
      )
      |> IO.inspect()
      |> ExAws.request(region: region)
      |> IO.inspect()

      src_path
      |> ExAws.S3.Upload.stream_file()
      |> ExAws.S3.upload(bucket, dest_path,
        timeout: :infinity,
        content_type: "video/mp4",
        acl: :public_read
      )
      |> IO.inspect()
      |> ExAws.request(region: region)
      |> IO.inspect()
      |> handle_result(record)
    end)
  end

  def handle_result({:ok, _}, video) do
    cs = Video.changeset(video, %{uploaded: true})
    Repo.update(cs)
  end

  def handle_result({:error, er}), do: IO.inspect(er)
end

defmodule Images do
  import Ecto.Query, warn: false

  @images_per_page 20

  def parse_root(root_path, output_directory) do
    files =
      root_path
      |> File.ls!()

    Enum.flat_map(files, &parse_dir(&1, output_directory, Path.join(root_path, &1)))
  end

  def parse_dir(dir, dir, path) do
    path
    |> File.ls!()
    |> Enum.filter(&String.ends_with?(&1, "jpg"))
    |> Enum.map(&parse_image(Path.join(path, &1)))
  end

  def parse_dir(_dir, output_directory, path) do
    if File.dir?(path) do
      path
      |> File.ls!()
      |> Enum.flat_map(&parse_dir(&1, output_directory, Path.join(path, &1)))
    else
      []
    end
  end

  def parse_date(datetime) do
    [date, time] = String.split(datetime, " ")
    date = String.replace(date, ":", "-")
    {:ok, datetime, _} = DateTime.from_iso8601("#{date}T#{time}-0400")
    datetime
  end

  def parse_tags(tags), do: String.split(tags, ",") |> Enum.map(&String.trim/1)

  def parse_image(path) do
    {:ok, data} = Exexif.exif_from_jpeg_file(path)
    tags = parse_tags(data.image_description)
    datetime = parse_date(data.datetime_original)
    {hash, file_path} = get_path(path, datetime)

    i =
      case Repo.one(from(i in Media.Image, where: i.original_path == ^path)) do
        nil -> %Media.Image{uploaded: false}
        im -> im
      end

    cs =
      Media.Image.changeset(i, %{
        datetime: datetime,
        flash: data.exif.flash,
        contrast: data.exif.contrast,
        focal_length: data.exif.focal_length,
        exposure_time: data.exif.exposure_time,
        white_balance: data.exif.white_balance,
        width: data.exif.exif_image_width,
        height: data.exif.exif_image_height,
        iso: get_in(data, [:exif, :iso_speed_ratings]) || 100,
        aperture: data.exif.f_number,
        orientation: data.orientation,
        path: file_path,
        original_path: path,
        hash: hash,
        tags: tags,
        uploaded: i.uploaded
      })

    if Ecto.Changeset.changed?(cs, :hash) do
      Ecto.Changeset.put_change(cs, :uploaded, false)
    else
      cs
    end
  end

  def get_path(image, datetime) do
    data = File.read!(image)
    hash = :crypto.hash(:md5, data)
    encoded = Base.encode16(hash, case: :lower)
    file = encoded <> ".jpg"

    {encoded,
     "/#{datetime.year}/#{String.pad_leading("#{datetime.month}", 2, "0")}/#{String.pad_leading("#{datetime.day}", 2, "0")}/#{file}"}
  end

  def upload_new(images, bucket, region) do
    new = Enum.reject(images, & &1.uploaded)

    uf = fn %{original_path: src_path, path: dest_path} = record ->
      data = File.read!(src_path)
      {:ok, thumb} = Image.from_binary(data)
      {:ok, thumb} = Image.thumbnail(thumb, 400, crop: :attention, fit: :cover)
      {:ok, thumb} = Image.write(thumb, :memory, suffix: ".jpg", minimize_file_size: true)
      thumb_path = Media.Image.thumb_path(record)

      bucket
      |> ExAws.S3.put_object(thumb_path, thumb,
        content_type: "image/jpeg",
        acl: :public_read
      )
      |> IO.inspect()
      |> ExAws.request(region: region)
      |> IO.inspect()

      bucket
      |> ExAws.S3.put_object(dest_path, data,
        content_type: "image/jpeg",
        acl: :public_read
      )
      |> IO.inspect()
      |> ExAws.request(region: region)
      |> IO.inspect()
      |> handle_upload_result(record)
    end

    new
    |> Task.async_stream(uf, max_concurrency: 3, timeout: :infinity)
    |> Stream.run()
  end

  def handle_upload_result({:ok, _}, image) do
    cs = Media.Image.changeset(image, %{uploaded: true})
    Repo.update(cs)
  end

  def handle_upload_result({:error, er}), do: IO.inspect(er)

  def group_by_tags(images) do
    Enum.reduce(images, %{}, fn i, acc ->
      Enum.reduce(i.tags, acc, fn t, a ->
        Map.update(a, t, [i], fn e -> e ++ [i] end)
      end)
    end)
  end

  def group_by_dates(images) do
    Enum.reduce(images, %{}, fn i, acc ->
      month = i.datetime.month |> to_string() |> String.pad_leading(2, "0")
      day = i.datetime.day |> to_string() |> String.pad_leading(2, "0")
      date = "#{i.datetime.year}-#{month}-#{day}"

      Map.update(acc, date, [i], fn e -> e ++ [i] end)
    end)
  end

  def create_index(images, tags, dates, media_server) do
    f =
      EEx.eval_file("templates/index.html.eex",
        images: Enum.slice(images, 0..@images_per_page),
        tags: tags,
        dates: dates,
        media_server: media_server,
        current_tag: "latest"
      )

    File.write!("../www/index.html", f)
    File.write!("../www/photography/tags/latest.html", f)
    File.write!("../www/photography/index.html", f)
  end

  def create_tags(images, tags, dates, media_server) do
    Enum.each(images, fn {k, v} ->
      f =
        EEx.eval_file("templates/index.html.eex",
          images: v,
          tags: tags,
          dates: dates,
          media_server: media_server,
          current_tag: k
        )

      File.write!("../www/photography/tags/#{slug(k)}.html", f)
    end)
  end

  def create_dates(images, tags, dates, media_server) do
    Enum.each(images, fn {k, v} ->
      f =
        EEx.eval_file("templates/index.html.eex",
          images: v,
          tags: tags,
          dates: dates,
          media_server: media_server,
          current_tag: k
        )

      File.write!("../www/photography/dates/#{slug(k)}.html", f)
    end)
  end

  def create_images(images, tags, dates, media_server) do
    Enum.each(images, fn v ->
      f =
        EEx.eval_file("templates/index.html.eex",
          images: [v],
          tags: tags,
          dates: dates,
          media_server: media_server,
          current_tag: v.hash
        )

      File.write!("../www/photography/images/#{v.hash}.html", f)
    end)
  end

  def rss(images, media_server) do
    items =
      Enum.map(images, fn i ->
        %{
          title: i.hash,
          description: description(i, media_server),
          link: link(i),
          hash: i.hash,
          datetime: i.datetime
        }
      end)

    f =
      EEx.eval_file("templates/rss.xml.eex",
        title: "Entropealabs Photography",
        description: "Photography by Entropealabs, LTD",
        link: "https://media.entropealabs.com/photography",
        items: items
      )

    File.write!("../www/photography.xml", f)
  end

  defp description(i, media_server) do
    tags = Enum.join(i.tags, ", ")

    """
      <img src="#{media_server}#{Media.Image.thumb_path(i)}" />
      <p>ƒ #{i.aperture} | #{Media.Image.secs(i.exposure_time)} | #{i.focal_length}mm | ISO#{i.iso}</p>
      <p>#{tags}</p>
    """
  end

  defp link(%{hash: hash}) do
    "https://media.entropealabs.com/photography/images/#{hash}"
  end

  def slug(text), do: String.replace(text, " ", "-")
end

defmodule Main do
  def main do
    children = [
      Repo
    ]

    _ok = Repo.__adapter__().storage_up(Repo.config())

    {:ok, _} = Supervisor.start_link(children, strategy: :one_for_one)

    Ecto.Migrator.run(Repo, [{0, Migration}, {1, Migration0}], :up,
      all: true,
      log_migrations_sql: :debug
    )
  end
end

root_path = System.fetch_env!("IMAGE_ROOT_PATH")
video_root_path = System.fetch_env!("VIDEO_ROOT_PATH")
output_directory = System.fetch_env!("IMAGE_OUTPUT_DIRECTORY")
bucket = System.fetch_env!("AWS_BUCKET_NAME")
region = System.fetch_env!("AWS_REGION")
media_server = System.fetch_env!("MEDIA_SERVER")

Main.main()

images =
  root_path
  |> Images.parse_root(output_directory)
  |> Enum.map(&Repo.insert_or_update!/1)
  |> Enum.sort_by(& &1.datetime, {:desc, DateTime})

tags = Images.group_by_tags(images)
dates = Images.group_by_dates(images)

tag_keys = tags |> Map.keys() |> List.insert_at(0, "latest") |> Enum.sort(:asc)
date_keys = dates |> Map.keys() |> Enum.sort(:desc)

Images.create_index(images, tag_keys, date_keys, media_server)
Images.create_images(images, tag_keys, date_keys, media_server)
Images.create_tags(tags, tag_keys, date_keys, media_server)
Images.create_dates(dates, tag_keys, date_keys, media_server)
Images.rss(Enum.slice(images, 0..25), media_server)
Images.upload_new(images, bucket, region)

videos =
  video_root_path
  |> Videos.parse_root()
  |> Enum.map(&Repo.insert_or_update!/1)
  |> Enum.sort_by(& &1.datetime, {:desc, DateTime})

Videos.create_videos(videos, media_server)
Videos.rss(videos, media_server)
Videos.upload_new(videos, bucket, region)
